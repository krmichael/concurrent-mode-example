import React, { Suspense } from "react";

//import Wait from "./Wait";

//const Hello = React.lazy(() => import("./Hello"));

//import About from "./About";

import { fetchData } from "./Api";

const resource = fetchData();

function About() {
  const about = resource.about.read();

  return <h2>{about}</h2>;
}

function Address() {
  const address = resource.address.read();

  return <h2>{address}</h2>;
}

function App() {
  return (
    <div>
      <Suspense fallback={<h2>Loading...</h2>}>
        <About />
        <Address />
      </Suspense>
    </div>
  );
}

export default App;
