export function fetchData() {
  let aboutPromise = fetchAbout();
  let addressPromise = fetchAddress();

  return {
    about: wrapPromise(aboutPromise),
    address: wrapPromise(addressPromise),
  };

  function fetchAbout() {
    return new Promise((resolve, reject) => {
      resolve("This is the about page with company details");
    }, 6000);
  }

  function fetchAddress() {
    return new Promise((resolve, reject) => {
      resolve("214, Brásilia, DF");
    }, 6000);
  }

  function wrapPromise(promise) {
    let status = "pending";
    let result;

    let suspender = promise.then(
      (data) => {
        status = "success";
        result = data;
      },
      (error) => {
        status = "error";
        result = error;
      }
    );

    return {
      read() {
        switch (status) {
          case "pending":
            throw suspender;

          case "error":
            throw result;

          default:
            return result;
        }
      },
    };
  }
}
