import React, { useState, useEffect } from "react";

import Address from "./Address";

export default function About() {
  const [content, setContent] = useState("");

  useEffect(() => {
    const getContent = new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve("This is the about page with company details");
      }, 6000);
    });

    getContent.then((content) => setContent(content));
  }, []);

  if (!content) {
    return <h2>Loading...</h2>;
  }

  return (
    <>
      <h2>{content}</h2>

      <Address />
    </>
  );
}
