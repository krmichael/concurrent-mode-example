import React, { useState, useEffect } from "react";

export default function Address() {
  const [address, setAddress] = useState("");

  useEffect(() => {
    const getAddress = new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve("214, Brásilia, DF");
      }, 6000);
    });

    getAddress.then((address) => setAddress(address));
  }, []);

  if (!address) {
    return <h2>Loading...</h2>;
  }

  return (
    <>
      <h2>{address}</h2>
    </>
  );
}
