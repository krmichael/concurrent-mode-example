import React from "react";

export default function Wait() {
  return <h2>Loading message.!</h2>;
}
